const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

// txtFirstName.addEventListener("keyup", (event) => {
//   spanFullName.innerHTML = txtFirstName.value;
//   console.log(event.target);
//   console.log(event.target.value);
// });

const fullNameFunction = () => {
  spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`;
};

txtFirstName.addEventListener("keyup", fullNameFunction);
txtLastName.addEventListener("keyup", fullNameFunction);
